<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
class Vishalgaikwad_Commercebug_Model_System_Config_Source_Log_Format_Classes
{
    public function toOptionArray()
    {
        return array(
        'Vishalgaikwad_Commercebug_Helper_Formatlog_Raw'=>'Raw JSON Notation',
        'Vishalgaikwad_Commercebug_Helper_Formatlog_Raw'=>'All Information, Simple Text',
        'custom'=>'Custom Class'
        );
    }
}